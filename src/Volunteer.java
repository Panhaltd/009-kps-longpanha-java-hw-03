public class Volunteer extends StaffMember implements Comparable<StaffMember>{
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }
    @Override
    public double pay() {
        return 0;
    }
    @Override
    public String toString() {
        return "\nID: "+id+
                "\nName : "+name+
                "\nAddress : "+address+
                "\nThanks!"+
                "\n------------------------------------";
    }

    @Override
    public int compareTo(StaffMember s) {
        return name.compareTo(s.name);
    }
}
