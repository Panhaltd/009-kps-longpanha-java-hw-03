public class HourlyEmployee extends StaffMember implements Comparable<StaffMember>{
    int hoursWorked;
    double rate;

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "\nID: "+id+
                "\nName : "+name+
                "\nAddress : "+address+
                "\nHour Worked : "+hoursWorked+
                "\nRate : "+rate+
                "\nPayment : "+pay()+
                "\n------------------------------------";
    }

    @Override
    public double pay() {
        return hoursWorked*rate;
    }
    @Override
    public int compareTo(StaffMember s) {
        return name.compareTo(s.name);
    }
}
